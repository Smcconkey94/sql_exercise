--1
SELECT *
FROM Trade t
JOIN position p ON p.opening_trade_id = t.id OR p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'mrk'


--2
SELECT SUM(t.size * t.price * (CASE WHEN t.buy = 0 
									THEN 1 
									ELSE -1 end)) as profit_loss
FROM trade t
JOIN position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
WHERE p.trader_id = 1
AND t.stock = 'mrk'
AND p.closing_trade_id <> p.opening_trade_id
AND p.closing_trade_id is not null;

--3
Create view all_profit_loss as
	SELECT g.first_name, g.last_name, SUM(t.size * t.price * (CASE WHEN t.buy = 0 
									THEN 1 
									ELSE -1 end)) as profit_loss
	FROM trade t
	JOIN position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
	JOIN trader g on p.trader_id = g.id
	WHERE p.closing_trade_id <> p.opening_trade_id
	AND p.closing_trade_id is not null
	GROUP BY g.first_name, g.last_name

--Test View
select * from all_profit_loss