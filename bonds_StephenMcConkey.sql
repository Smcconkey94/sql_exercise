--1
select * 
from bond
where cusip = '28717RH95'
--2
select *
from bond
order by maturity asc
--3
select sum(quantity*price) as total_portfolio_value 
from bond
--4
select b.cusip,  Year(b.maturity) as year, sum(quantity*(coupon/100)) as annual_return
from bond b
group by b.cusip,b.maturity 
--5
select * 
from bond b
join rating r on b.rating = r.rating
where r.ordinal <= 3;
--6
select rating, avg(price) as average_price, avg(coupon) as average_coupon from bond group by rating
--7
select CUSIP, sum(coupon/price) as Yield, r.expected_yield as Expected_Yield
from bond
join rating r on bond.rating = r.rating
group by cusip, r.expected_yield
having sum(coupon/price) < r.expected_yield

